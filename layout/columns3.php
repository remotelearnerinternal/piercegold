<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package   theme_piercegold
 * @copyright 2013 Moodle, moodle.org
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// Get the HTML for the settings bits.
$html = theme_piercegold_get_html_for_settings($OUTPUT, $PAGE);

if (right_to_left()) {
    $regionbsid = 'region-bs-main-and-post';
} else {
    $regionbsid = 'region-bs-main-and-pre';
}

echo $OUTPUT->doctype() ?>
<!--[if lte IE 7]> <html class="ie7" <?php echo $OUTPUT->htmlattributes() ?>> <![endif]-->
<!--[if IE 8]>     <html class="ie8" <?php echo $OUTPUT->htmlattributes() ?>> <![endif]-->
<!--[if IE 9]>     <html class="ie9" <?php echo $OUTPUT->htmlattributes() ?>> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html <?php echo $OUTPUT->htmlattributes() ?>> <!--<![endif]-->
<head>
    <title><?php echo $OUTPUT->page_title(); ?></title>
    <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
    <?php echo $OUTPUT->standard_head_html() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Alex+Brush' rel='stylesheet' type='text/css'>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
</head>

<body <?php echo $OUTPUT->body_attributes(); ?>>
<div id="bg-image"></div>
<?php echo $OUTPUT->standard_top_of_body_html() ?>

<header role="banner" class="navbar navbar-fixed-top <?php echo $html->navbarclass ?>">
    <div id="headerwrap" class="container">
        <div id="logo">
            <a href="<?php echo $CFG->wwwroot;?>"><img src="<?php echo $OUTPUT->pix_url('logo', 'theme'); ?>"></a>
        </div>
        <nav role="navigation" class="navbar-inner">
            <div>
                <a class="btn btn-navbar" data-toggle="workaround-collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="nav-collapse collapse">
                    <ul id="social">
                        <li><a href="#" title="Like us on Facebook"><i class="icon-facebook-sign"></i></a></li>
                        <li><a href="#" title="Follow us on Twitter"><i class="icon-twitter-sign"></i></a></li>
                        <li><a href="#" title="Get Help"><i class="icon-question-sign"></i></a></li>
                    </ul>
                    <?php echo $OUTPUT->custom_menu(); ?>
                    <ul class="nav pull-right">
                        <li><?php echo $OUTPUT->page_heading_menu(); ?></li>
                        <li class="navbar-text"><?php echo $OUTPUT->login_info() ?></li>
                        <li class="profilepic">
                            <?php
                                if (isloggedin()) {
                                    echo $OUTPUT->user_picture($USER);
                                }
                            ?>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>

<div id="pagewrap">
<div id="page" class="container">
    <header id="page-header" class="clearfix">
        <div id="page-navbar" class="clearfix">
            <div class="breadcrumb-nav"><?php echo $OUTPUT->navbar(); ?></div>
            <nav class="breadcrumb-button"><?php echo $OUTPUT->page_heading_button(); ?></nav>
        </div>
        <?php echo $html->heading; ?>
        <div id="course-header">
            <?php echo $OUTPUT->course_header(); ?>
        </div>
    </header>

    <div id="page-content" class="row-fluid">
        <div id="<?php echo $regionbsid ?>" class="span9">
            <div class="row-fluid">
                <section id="region-main" class="span8 pull-right">
                    <?php
                    echo $OUTPUT->course_content_header();
                    echo $OUTPUT->main_content();
                    echo $OUTPUT->course_content_footer();
                    ?>
                </section>
                <?php echo $OUTPUT->blocks('side-pre', 'span4 desktop-first-column'); ?>
            </div>
        </div>
        <?php echo $OUTPUT->blocks('side-post', 'span3'); ?>
    </div>

    <?php echo $OUTPUT->standard_end_of_body_html() ?>
</div>
</div>

<footer id="page-footer">
    <div id="footer-content" class="container">
        <a class="brand" href="<?php echo $CFG->wwwroot;?>"><img src="<?php echo $OUTPUT->pix_url('logo-footer', 'theme'); ?>"></a>
        <ul>
            <li>6201 Winnetka Avenue</li>
            <li>Woodland Hills, California 91371</li>
            <li>Phone: 818-719-6401</li>
            <li><a href="http://www.piercecollege.edu/">www.piercecollege.edu</a></li>
            <li>&copy; 2013 Pierce College</li>
        </ul>
        <div id="course-footer"><?php echo $OUTPUT->course_footer(); ?></div>
    </div>
</footer>

</body>
</html>
