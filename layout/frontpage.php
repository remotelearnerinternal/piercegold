<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package   theme_piercegold
 * @copyright 2013 Moodle, moodle.org
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// Get the HTML for the settings bits.
$html = theme_piercegold_get_html_for_settings($OUTPUT, $PAGE);

if (right_to_left()) {
    $regionbsid = 'region-bs-main-and-post';
} else {
    $regionbsid = 'region-bs-main-and-pre';
}

echo $OUTPUT->doctype() ?>
<!--[if lte IE 7]> <html class="ie7" <?php echo $OUTPUT->htmlattributes() ?>> <![endif]-->
<!--[if IE 8]>     <html class="ie8" <?php echo $OUTPUT->htmlattributes() ?>> <![endif]-->
<!--[if IE 9]>     <html class="ie9" <?php echo $OUTPUT->htmlattributes() ?>> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html <?php echo $OUTPUT->htmlattributes() ?>> <!--<![endif]-->
<head>
    <title><?php echo $OUTPUT->page_title(); ?></title>
    <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
    <?php echo $OUTPUT->standard_head_html() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Alex+Brush' rel='stylesheet' type='text/css'>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
</head>

<body <?php echo $OUTPUT->body_attributes(); ?>>
<div id="bg-image"></div>

<?php echo $OUTPUT->standard_top_of_body_html() ?>

<header role="banner" class="navbar navbar-fixed-top <?php echo $html->navbarclass ?>">
    <div id="headerwrap" class="container">
        <div id="logo">
            <a href="<?php echo $CFG->wwwroot;?>"><img src="<?php echo $OUTPUT->pix_url('logo', 'theme'); ?>"></a>
        </div>
        <nav role="navigation" class="navbar-inner">
            <div>
                <a class="btn btn-navbar" data-toggle="workaround-collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="nav-collapse collapse">
                    <ul id="social">
                        <li><a href="https://www.facebook.com/PierceCollege" title="Like us on Facebook"><i class="icon-facebook-sign"></i></a></li>
                        <li><a href="https://twitter.com/PierceCollegeCA" title="Follow us on Twitter"><i class="icon-twitter-sign"></i></a></li>
                        <li><a href="http://moodle.piercecollege.edu/file.php/1/StudentHelpDesk/StudentHelpFAQ.html" title="Get Help"><i class="icon-question-sign"></i></a></li>
                    </ul>
                    <?php echo $OUTPUT->custom_menu(); ?>
                    <ul class="nav pull-right">
                        <li><?php echo $OUTPUT->page_heading_menu(); ?></li>
                        <li class="navbar-text"><?php echo $OUTPUT->login_info() ?></li>
                        <li class="profilepic">
                            <?php
                                if (isloggedin()) {
                                    echo $OUTPUT->user_picture($USER);
                                }
                            ?>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>
<div id="layerslider-wrap" >
    <div id="layerslider-container" class="container">
    <div id="layerslider" style="width: 1170px; height: 469px; margin: 0px auto; ">
            <div class="ls-layer" style="transition2d: 5; ">
                <img src="<?php echo $OUTPUT->pix_url('banner1', 'theme'); ?>" style="slidedirection : fade; slideoutdirection : fade; scalein : 0.5; scaleout : 0.5;" class="ls-bg" alt="Slide background">
                <img class="ls-s2" src="<?php echo $OUTPUT->pix_url('banner2', 'theme'); ?>" alt="Pierce College" style="slidedirection : fade; slideoutdirection : fade;  delayin : 5000; showuntil : 10000; ">
                <img class="ls-s2" src="<?php echo $OUTPUT->pix_url('banner3', 'theme'); ?>" alt="Pierce College" style="slidedirection : fade; slideoutdirection : fade;  delayin : 10000; showuntil : 10000; ">
                <img class="ls-s2" src="<?php echo $OUTPUT->pix_url('banner1', 'theme'); ?>" alt="Pierce College" style="slidedirection : fade; slideoutdirection : fade;  delayin : 15000; showuntil : 10000; ">
                <img class="ls-s2" src="<?php echo $OUTPUT->pix_url('banner2', 'theme'); ?>" alt="Pierce College" style="slidedirection : fade; slideoutdirection : fade;  delayin : 20000; showuntil : 10000; ">
                <img class="ls-s2" src="<?php echo $OUTPUT->pix_url('banner3', 'theme'); ?>" alt="Pierce College" style="slidedirection : fade; slideoutdirection : fade;  delayin : 25000; showuntil : 10000; ">

                <h1 class="ls-s3 text4" style="position: absolute; top:61%; left: 661px; slidedirection : fade; slideoutdirection : fade; durationin : 2500; durationout : 2500; easingin : easeInOutQuint; easingout : easeInOutQuint; delayin : 1500; delayout : 0; showuntil : 99999999999;">Welcome to</h1>
                <h1 class="ls-s3 text5" style="position: absolute; top:79%; left: 661px; slidedirection : fade; slideoutdirection : fade; durationin : 2500; durationout : 2500; easingin : easeInOutQuint; easingout : easeInOutQuint; delayin : 2000; delayout : 0; showuntil : 99999999999;">Pierce Online</h1>

                <h5 class="ls-s-3 text6" style="delayin : 400; top:97px; left: 0px; slidedirection : fade; slideoutdirection : fade; durationout : 750; easingin : easeOutQuint; scalein : .8; scaleout : .8;">getting started:</h5>


                <h5 class="ls-s-3 text7" style="delayin : 700; top:153px; left: 0px; slidedirection : right; slideoutdirection : fade; durationout : 750; easingin : easeOutQuint; scalein : .8; scaleout : .8;">
                    <a href="<?php echo $CFG->wwwroot; ?>/mod/page/view.php?id=217920"><i class="icon-group"></i>Students</a>
                </h5>

                <h5 class="ls-s-3 text8" style="delayin : 1000; top:217px; left: 0px; slidedirection : right; slideoutdirection : fade; durationout : 750; easingin : easeOutQuint; scalein : .8; scaleout : .8;">
                    <a href="<?php echo $CFG->wwwroot; ?>/mod/page/view.php?id=217917"><i class="icon-user"></i>Faculty</a>
                </h5>

                <h5 class="ls-s-3 text9" style="delayin : 1300; top:281px; left: 0px; slidedirection : right; slideoutdirection : fade; durationout : 750; easingin : easeOutQuint; scalein : .8; scaleout : .8;">
                    <a href="<?php echo $CFG->wwwroot; ?>/mod/page/view.php?id=217918"><i class="icon-check"></i>Requirements</a>
                </h5>

                <h5 class="ls-s-3 text10" style="delayin : 1600; top:345px; left: 0px; slidedirection : right; slideoutdirection : fade; durationout : 750; easingin : easeOutQuint; scalein : .8; scaleout : .8;">
                    <a href="<?php echo $CFG->wwwroot; ?>/mod/page/view.php?id=217919"><i class="icon-question-sign"></i>FAQs</a>
                </h5>
            </div>

        </div>
    </div>
</div>

<div id="pagewrap">
    <div id="page" class="container">
        <div id="page-content" class="row-fluid">
            <div id="<?php echo $regionbsid ?>" class="span9">
                <div class="row-fluid">
                    <section id="region-main" class="span8 pull-right">
                        <?php
                            echo $OUTPUT->course_content_header();
                            echo $OUTPUT->main_content();
                            echo $OUTPUT->course_content_footer();
                        ?>
                    </section>
                    <?php echo $OUTPUT->blocks('side-pre', 'span4 desktop-first-column'); ?>
                </div>
            </div>
            <div id="helpdesk" class="span3"><a href="<?php echo $CFG->wwwroot; ?>/file.php/1/StudentHelpDesk/StudentHelpFAQ.html"><img src="<?php echo $OUTPUT->pix_url('helpdesk', 'theme'); ?>" alt="Student Help Desk"></a></div>
            <?php echo $OUTPUT->blocks('side-post', 'span3'); ?>
        </div>

        <?php echo $OUTPUT->standard_end_of_body_html() ?>
    </div>
</div>
<footer id="page-footer">
    <div id="footer-content" class="container">
        <a class="brand" href="<?php echo $CFG->wwwroot;?>"><img src="<?php echo $OUTPUT->pix_url('logo-footer', 'theme'); ?>"></a>
        <ul>
            <li>6201 Winnetka Avenue</li>
            <li>Woodland Hills, California 91371</li>
            <li>Phone: 818-719-6401</li>
            <li><a href="http://www.piercecollege.edu/">www.piercecollege.edu</a></li>
            <li>&copy; 2013 Pierce College</li>
        </ul>
        <div id="course-footer"><?php echo $OUTPUT->course_footer(); ?></div>
    </div>
</footer>
</body>
</html>