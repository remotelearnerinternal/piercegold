<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'theme_piercegold', language 'en'
 *
 * @package   theme_piercegold
 * @copyright  2013 Remote-Learner
 * @license    http://www.remote-learner.net/
 */

$string['choosereadme'] = '
<div class="clearfix">
<div class="well">
<h2>Piercegold Custom Theme</h2>
<h3>About</h3>
<p>Piercegold is a custom theme built by Remote-Learner.net.</p>
<h3>Custom Theme Credits</h3>
<p>Author: Ryan DeBerardinis<br>
Contact: rdeber@gmail.com<br>
Website: <a href="http://www.ryandeberardinis.com">www.ryandeberardinis.com</a>
</p>
</div></div>';

$string['configtitle'] = 'piercegold';
$string['pluginname'] = 'Piercegold';

$string['region-side-post'] = 'Right';
$string['region-side-pre'] = 'Left';

